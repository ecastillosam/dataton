@extends('layouts.app')

@section('content')
<!--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->

        
          <section class="login_content">
            <form method="POST" action="{{ route('register') }}">
                @csrf
              <h1>Crear una cuenta</h1>
              <div>
                <input type="text" name="name" class="form-control" placeholder="Nombre completo" required />
              </div>
              <div>
                <input type="number" name="edad" class="form-control" min="18" max="150" placeholder="Edad" required />
              </div><br>
              <div>
                    <select name="perfil" class="form-control" required>
                        <option>Elegir un perfil</option>
                        <option value="1">Usuario</option>
                    </select>
                </div><br>
              <div>
                <input type="email" name="email" class="form-control" placeholder="Correo electrónico" required />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Contraseña" required />
              </div>
              <div>
                <input type="password" name="password_confirmation" class="form-control" placeholder="Confirmar contraseña" required />
              </div>
              <div>
                <button type="submit" class="btn btn-success">{{ __('Registrarse') }}</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">¿ Ya eres un miembro ?
                  <a href="{{ route('login') }}" class="to_register">Iniciar sesión</a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <p>Dataton 2021</p>
                </div>
              </div>
            </form>
          </section>
        
@endsection
