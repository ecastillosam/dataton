@extends('layouts.app2')

@section('content')
<div class="">
            <div class="page-title">
            <div class="clearfix"></div>

            <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Información de búsqueda</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form id="form-dataton" class="form-horizontal form-label-left" method="POST" action="{{ route('dataton.store') }}">
                      @csrf
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Capturar R.F.C.</label>
                        <div class="col-sm-9">
                          <div class="input-group">
                            <input type="text" name="buscarfc" id="buscarfc" class="form-control" placeholder="Ejemplo: CARE780612">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-success">Buscar</button>
                            </span>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Datos del servidor público</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left input_mask">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Declaración:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small>Sin información</small></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Institución:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small>Sin información</small></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre completo:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small>Sin información</small></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">R.F.C.:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small>Sin información</small></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ingresos:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small><span class="label label-danger">Sin información</span></small></h4>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Patrominio:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small><span class="label label-warning">Sin información</span></small></h4>
                        </div>
                      </div>                      
                      <div class="ln_solid"></div>
                    </form>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Disparidad representada entre ingresos y patrimonio declarado</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left">

                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">Sin información</div>
                      </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>

            </div>



          </div>
@endsection
