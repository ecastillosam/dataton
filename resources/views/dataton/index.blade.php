@extends('layouts.app2')

@section('content')
<div class="">
            <div class="page-title">
            <div class="clearfix"></div>

            <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Información de búsqueda</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form id="form-dataton" class="form-horizontal form-label-left" method="POST" action="{{ route('dataton.store') }}">
                     @csrf
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Capturar RFC</label>
                        <div class="col-sm-9">
                        <div class="input-group">
                            <input type="text" name="buscarfc" id="buscarfc" class="form-control" placeholder="Ejemplo: CARE780612">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-success">Buscar</button>
                            </span>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Datos generales del servidor público</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left input_mask">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Declaración:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small>{{ $informacionSP[0]["tipo"] }}</small></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Institución:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small>{{ $informacionSP[0]["institucion"] }}</small></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre completo:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small>{{ $informacionSP[0]["nombre"] }}</small></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">R.F.C.:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small>{{ $informacionSP[0]["rfc"] }}</small></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ingresos:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small><span class="label label-danger">{{ $informacionSP[0]["ingresos"] }}</span></small></h4>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Patrominio:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <h4><small><span class="label label-warning">{{ $informacionSP[0]["gastos"] }}</span></small></h4>
                        </div>
                      </div>                                            
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="alert alert-warning">
                            <ul class="fa-ul">
                              <li>
                                <i class="fa fa-info-circle fa-lg fa-li"></i> La información corresponde al (los) ejercicio(s): @foreach($ejercioDeclarado as $key => $val) @php echo $val.",";  @endphp @endforeach</small></h4>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>                      
                    </form>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                <div class="row">
                  <div class="x_title">
                    <h2>Disparidad representada entre ingresos y patrimonio declarado</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left">
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <canvas id="myChart" width="400" height="400"></canvas>
                        </div>
                      </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

            </div>

          </div>

          @push('jscustom')
<script>
var datos=[];

  /* $(document).ready(function(){

      datos[0]=formulario.ingreso.value;
      datos[1]=formulario.patrimonio.value;

   });*/
   
const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Ingreso', 'Patrimonio'],
        datasets: [{
            label: 'Ingreso',
            data: [@php echo $informacionSP[0]["ingresos"] @endphp, @php echo $informacionSP[0]["gastos"] @endphp],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(255, 206, 86, 1)'               
            ],
            borderWidth: 1
        },
        {
    type: 'line',
    label: 'Patrimonio',
    fill: false,
    backgroundColor: [
                'rgba(255, 206, 86, 0.2)',
    ],   
    borderColor: ['rgba(255, 206, 86, 1)',],
    borderWidth: 1
  }        
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>

@endpush          
@endsection
