<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Prueba;

class DatatonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tmp = Prueba::all();

        $cont = 0;
        $ingresos = 0;
        $buscar_rfc = $request->buscarfc;
        //$buscar_rfc = "CASC850118";
        //$buscar_rfc = "CÓGP760116";
        $gastos = 0;
        $informacionSP = array();
        $bandNombre = 1;
        $ejercioDeclarado = array();
        foreach($tmp as $info){
                $tipo = $info->metadata["tipo"];
                $rfc = $info->declaracion["situacionPatrimonial"]["datosGenerales"]["rfc"]["rfc"];

                $bienesInmuebles = 0;
                $elementos = 0;
                if(($rfc == $buscar_rfc) && ($tipo=="MODIFICACIÓN")){
                    $marcaTiempo = $info->metadata["actualizacion"];
                    $tmpEjercicio = explode("T",$marcaTiempo);
                    $ejercicio = explode("-",$tmpEjercicio[0]);
                    $ejercioDeclarado [] = $ejercicio[0];
                    if($bandNombre){
                        $tipoAux = $tipo;
                        $nombreCompleto = $info->declaracion["situacionPatrimonial"]["datosGenerales"]["nombre"];
                        $nombreCompleto .=" ".$info->declaracion["situacionPatrimonial"]["datosGenerales"]["primerApellido"];
                        $nombreCompleto .=" ".$info->declaracion["situacionPatrimonial"]["datosGenerales"]["segundoApellido"];
                        $institucion = $info->metadata["institucion"];
                        $bandNombre = 0;
                    }
                    $bienesInmuebles = $info->declaracion["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"];
                    $vehiculos = $info->declaracion["situacionPatrimonial"]["vehiculos"]["vehiculo"];
                    $bienesMuebles = $info->declaracion["situacionPatrimonial"]["bienesMuebles"]["bienMueble"];
                    $adeudos = $info->declaracion["situacionPatrimonial"]["adeudos"]["adeudo"];                    
                    $inversiones = $info->declaracion["situacionPatrimonial"]["inversiones"]["inversion"];

                    #Obtencion de los gastos en bienes inmuebles.
                    $elementosbIn = count($bienesInmuebles);
                    for($i = 0; $i < $elementosbIn; $i++){
                        $formaPago = $bienesInmuebles[$i]["formaPago"];
                        if($formaPago != "NO APLICA"){
                            $gastos = $gastos + $bienesInmuebles[$i]["valorAdquisicion"];
                        }
                    }

                    #Obtencion de los gastos en vehiculos/otros.
                    $elementosVe = count($vehiculos);
                    for($i = 0; $i < $elementosVe; $i++){
                        $tipoOperacion = $vehiculos[$i]["tipoOperacion"];
                        if($tipoOperacion != "NO APLICA"){
                            $gastos = $gastos + $vehiculos[$i]["valorAdquisicion"]["valor"];
                        }
                    }

                    #Obtencion de los gastos en vehiculos/otros.
                    $elementosbMu = count($bienesMuebles);
                    for($i = 0; $i < $elementosbMu; $i++){
                        $tipoOperacion = $bienesMuebles[$i]["tipoOperacion"];
                        if($tipoOperacion != "NO APLICA"){
                            $gastos = $gastos + $bienesMuebles[$i]["valorAdquisicion"]["valor"];
                        }
                    }                    

                    #Obtencion de los adeudos.                  
                    $elementosAd = count($adeudos);
                    for($i = 0; $i < $elementosAd; $i++){
                        $tipoOperacion = $adeudos[$i]["tipoOperacion"];
                        if($tipoOperacion != "NO APLICA"){
                            $gastos = $gastos + $adeudos[$i]["saldoInsolutoDiciembreAnterior"]["valor"];
                        }
                    }
                    
                    #Obtencion de las inversiones.
                    $elementosInv = count($inversiones);
                    for($i = 0; $i < $elementosInv; $i++){
                        $tipoOperacion = $inversiones[$i]["tipoOperacion"];
                        if($tipoOperacion != "NO APLICA"){
                            $gastos = $gastos + $inversiones[$i]["saldoDiciembreAnterior"]["valor"];
                        }
                    }                   

                    $ingresos = $ingresos + $info->declaracion["situacionPatrimonial"]["ingresos"]["totalIngresosAnualesNetos"]["valor"];
                    $cont++;
                }
        }

        $informacionSP[] = ["tipo"=>$tipoAux,"institucion"=>$institucion,"nombre"=>$nombreCompleto,"rfc"=>$buscar_rfc,"ingresos"=>$ingresos,"gastos"=>$gastos];
        asort($ejercioDeclarado);
        return view('dataton.index')->with(compact('informacionSP','ejercioDeclarado'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
